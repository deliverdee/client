var base_url="http://localhost:8888";

function register(){ 
    var fullname=$("#fullname").val();
    var email=$("#email").val();
    var password=$("#password").val();
    console.log(fullname);
    console.log(email);
    console.log(password);

    var settings = {
        "url": base_url+"/apiv1/public/auth",
        "method": "POST",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        "data": {
          "email": email,
          "fullname": fullname,
          "password": password
        }
      };
      
      $.ajax(settings).done(function (response) {
        console.log(response);
        if(response.status=="201"){
            alert('register succses, please login');
            window.location.replace("login.html");
        }else{
            alert(response);
        }
      });

    
}

function login(){
    var email=$("#email").val();
    var password=$("#password").val();

    var settings = {
        "url": base_url+"/apiv1/public/auth/"+email,
        "method": "GET",
        "timeout": 0,
      };
      
      $.ajax(settings).done(function (response) {
        console.log(response);
        console.log(response[0].password);
        console.log(password);
        if(response[0].password==password){

        localStorage.setItem("email", email);
        localStorage.setItem("password", password);
        localStorage.setItem("session", "abcde");
            
        var random = makeid(20);
        var hash = CryptoJS.SHA3(random, { outputLength: 224 });
        localStorage.setItem("session2", hash);
        check_session();

        } 

      }).fail(function(){ 
        alert('Invalid Email / password');
      });
    
}

// create random string for token
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
};

function logout(){
    localStorage.clear();
    session();
}

function blogs(){

    var settings = {
        "url": base_url+"/apiv1/public/blogs",
        "method": "GET",
        "timeout": 0,
      };
      
      $.ajax(settings).done(function (response) {
        console.log(response);
        $("#listblog").html("");
        $.each(response, function(index, value) {    
             var string = '<tr style="white-space: nowrap;text-align:right;">'+
             '<td>' + value.id+ '</td>'+
             '<td>' + value.title+ '</td>'+
             '<td>' + value.author+ '</td>'+
             '<td> <a class="btn btn-sm bg-warning text-dark" href="blog.html?id='+value.id+'" >view</a></td>'+
             '</tr>';
             $("#listblog").append(string);
         });
      });
}

function blog(){
    var id = getUrlParameter('id');
    $("#title").html("");
    $("#author").html("");
    $("#article").html("");
    var settings = {
    "url": base_url+"/apiv1/public/blogs/"+id,
    "method": "GET",
    "timeout": 0,
    };
    
    $.ajax(settings).done(function (response) {
        console.log(response);
        $("#title").append(response[0].title);
        $("#author").append(response[0].author);
        $("#article").append(response[0].article);
    });
}

function create(){

    var title=$("#title").val();
    var article=$("#article").val();
    var author=localStorage.getItem("email");

    var settings = {
        "url": base_url+"/apiv1/public/blogs",
        "method": "POST",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        "data": {
          "title": title,
          "article": article,
          "author": author
        }
      };
      
    $.ajax(settings).done(function (response) {
        console.log(response);
        window.location.replace("blogs.html");
    });

   
}

function mutuals(){

}

function session(){
    var userid = localStorage.getItem("email");
    $("#userid").html(userid);
    var storedValue = localStorage.getItem("session");
    if(storedValue!="abcde"){
        window.location.replace("login.html");
    }
}

function check_session(){
    var storedValue = localStorage.getItem("session");
    if(storedValue=="abcde"){
        window.location.replace("blogs.html");
    }
}

function check_follow(){
    var userid = localStorage.getItem("email");
    var author = $("#author").text();
    var id = getUrlParameter('id');
    if(userid!=author){
        var settings = {
            "url": base_url+"/apiv1/public/custom/check",
            "method": "POST",
            "timeout": 0,
            "headers": {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            "data": {
              "userid": author,
              "follower": userid
            }
          };
          
          $.ajax(settings).done(function (response) {
            console.log(response);
            if(response[0].count=="1"){
        
                var settings = {
                    "url": base_url+"/apiv1/public/custom/checkblog",
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                      "Content-Type": "application/x-www-form-urlencoded"
                    },
                    "data": {
                      "userid": userid,
                      "blogid": id
                    }
                  };
                  
                  $.ajax(settings).done(function (response) {
                    console.log(response);
                    if(response==0){
                        // alert('satu bro');
                        var settings = {
                            "url": base_url+"/apiv1/public/custom/readblog",
                            "method": "POST",
                            "timeout": 0,
                            "headers": {
                              "Content-Type": "application/x-www-form-urlencoded"
                            },
                            "data": {
                              "userid": userid,
                              "blogid": id
                            }
                          };
                          
                          $.ajax(settings).done(function (response) {
                            console.log(response);
                          });
                    }
                  });

                $("#follow_btn").empty();
                var thebtn='<button id="unfollow" class="btn btn-sm btn-info" onclick="unfollow();">Following</button>';
                $("#follow_btn").append(thebtn);

            } else {
           
                $("#follow_btn").empty();
                var thebtn='<button id="follow" class="btn btn-sm btn-warning" onclick="follow();">Follow</button>';
                $("#follow_btn").append(thebtn);
            }
          });

        
    }



  

}

function follow(){
    var userid = localStorage.getItem("email");
    var author = $("#author").text();
    var settings = {
        "url": base_url+"/apiv1/public/custom/follow",
        "method": "POST",
        "timeout": 0,
        "headers": {
        "Content-Type": "application/x-www-form-urlencoded"
        },
        "data": {
            "userid": author,
            "follower": userid
        }
    };
    
    $.ajax(settings).done(function (response) {
        console.log(response);
        $("#follow_btn").empty();
        var thebtn='<button id="unfollow" class="btn btn-sm btn-info" onclick="unfollow();" >Following</button>';
        $("#follow_btn").append(thebtn);
    });
   
}

function unfollow(){
    var userid = localStorage.getItem("email");
    var author = $("#author").text();
    var settings = {
        "url": base_url+"/apiv1/public/custom/unfollow",
        "method": "POST",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        "data": {
            "userid": author,
            "follower": userid
        }
    };
      
    $.ajax(settings).done(function (response) {
        console.log(response);
        $("#follow_btn").empty();
        var thebtn='<button id="follow" class="btn btn-sm btn-warning" onclick="follow();">Follow</button>';
        $("#follow_btn").append(thebtn);
    });
    
    
}
